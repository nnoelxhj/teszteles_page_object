﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using System.Collections;
using webui_beadando.PagesAtClass;
using webui_beadando.WidgetAtClass;
using webui_beadando.WidgetsAtClass;
using OpenQA.Selenium.Support.UI;
using System.Drawing.Imaging;
using NUnit.Framework.Interfaces;

namespace webui_beadando
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("TestData")]
        public void WishListFinalPriceCheckAfterAdding(String stuff, string price)
        {
            var result = new SearchWidget(Driver)
                    .DriverSetup() // url beallitasa + window handle
                    .SearchForObject(stuff)  // input beirasa es click a keresésre
                    .GetSearchResults()
                    .GetFirstSearchResult() // legelső találat kiválasztása
                    .GetTheCurrentPrice() // ár kinyerése
                    .AddToWishList() // kivánságlistához adás
                    .GoToWishList() // kivánságlistához ugrás
                    .GetTheWishListElementName(); // kivansaglistaban levo elem aranak kinyerése

            Assert.AreEqual(result, price);
        }

        [TearDown]
        public void TakeAScreenShot()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                string directory = Directory.GetCurrentDirectory();
                string screenshotName = directory + "//" + TestContext.CurrentContext.Test.MethodName + "_" + // test.name null-t ad vissza, de a methodname mar ad vissza "valamit"
                    DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + "_error.png";

                var screen = ((ITakesScreenshot)Driver).GetScreenshot();
                screen.SaveAsFile(screenshotName);
            }
        }

        static IEnumerable TestData()
        {
            var doc = XElement.Load("data.xml");

            return
                from vars in doc.Descendants("testData")
                let stuff = vars.Attribute("stuff").Value
                let price = vars.Attribute("price").Value
                select new object[] { stuff, price };
        }
    }
}
