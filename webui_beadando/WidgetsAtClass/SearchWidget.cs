﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webui_beadando.WidgetAtClass
{
    public class SearchWidget : BasePage
    {
        public SearchWidget(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement searchBar => Driver.FindElement(By.Id("searchboxTrigger"));

        public IWebElement searchButton => Driver.FindElement(By.CssSelector("Button[class='btn btn-default searchbox-submit-button']"));

        public void Input(String input)
        {
            searchBar.SendKeys(input);
        }

        public void PressSearch()
        {
            searchButton.Click();
        }

        public SearchWidget SearchForObject(String input)
        {
            this.Input(input);
            this.PressSearch();
            return new SearchWidget(Driver); // itt már lehet hogy searchresultwidget kene returnnek
        }

        public SearchWidget DriverSetup()
        {
            Driver.Url = "https://www.emag.hu/homepage";
            Driver.Manage().Window.Maximize();
            return new SearchWidget(Driver);
        }

        public SearchResultWidget GetSearchResults()
        {
            return new SearchResultWidget(Driver);
        }
    }
}
