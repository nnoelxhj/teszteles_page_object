﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webui_beadando.WidgetsAtClass
{
    public class FirstResultWidget : BasePage
    {
        public FirstResultWidget(IWebDriver driver) : base(driver)
        {

        }

        private string Price { get; set; }

        public bool SearchedObjectEqualsToTheWantedParameter(string price)
        {
            return this.Price == price;
        }

        public IWebElement goWishList => Driver.FindElement(By.Id("my_wishlist"));

        public IWebElement getPrice => Driver.FindElement(By.CssSelector("p[class='product-new-price']"));

        public IWebElement addToWishList => Driver.FindElement(By.CssSelector("Button[class='add-to-favorites btn btn-xl btn-default btn-icon btn-block gtm_t95ovv']"));
     
        public FirstResultWidget GetTheCurrentPrice()
        {
            this.Price = getPrice.Text;
            return new FirstResultWidget(Driver);
        }

        private void GoToWishListClick()
        {
            goWishList.Click();
        }

        private void ClickOnTheHeart()
        {
            addToWishList.Click();
        }

        private void WaitForNewElementToShow()
        {
            WebDriverWait ww = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            ww.Until(x => x.FindElement(By.CssSelector("div[class='table-cell col-xs-9']")));
            // várakozás, hogy a kedvencekhez adás után eddig rejtett notification megjelenjen
        }

        public FirstResultWidget AddToWishList()
        {
            this.ClickOnTheHeart();
            this.WaitForNewElementToShow();
            return new FirstResultWidget(Driver);
        }

        public WishListWidget GoToWishList()
        {
            this.GoToWishListClick();
            return new WishListWidget(Driver);
        }
    }
}
