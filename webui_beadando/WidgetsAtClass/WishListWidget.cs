﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webui_beadando.WidgetsAtClass
{
    public class WishListWidget : BasePage
    {
        public WishListWidget(IWebDriver driver) : base(driver)
        {

        }

        private string WishListElementPrice { get; set; }

        public bool WishListElementPriceCheck(string inputPrice)
        {
            return this.WishListElementPrice.Contains(inputPrice);
        }

        private void SetTheElementName(string name)
        {
            this.WishListElementPrice = name;
        }

        public IWebElement wishListPrice => Driver.FindElement(By.CssSelector("p[class='product-new-price']"));
        
        public string GetTheWishListElementName()
        {
            SetTheElementName(wishListPrice.Text);
            return WishListElementPrice;
        }
    }
}
