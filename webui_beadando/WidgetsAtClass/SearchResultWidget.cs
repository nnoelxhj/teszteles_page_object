﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webui_beadando.WidgetsAtClass;

namespace webui_beadando.WidgetAtClass
{
    public class SearchResultWidget : BasePage
    {
        public SearchResultWidget(IWebDriver driver ) : base(driver)
        {
        }

        public IWebElement firstResult => Driver.FindElement(By.ClassName("card-heading")); // legelső elem 

        public void SelectTheFirstResult()
        {
            firstResult.Click();
        }

        public FirstResultWidget GetFirstSearchResult()
        {
            this.SelectTheFirstResult();
            return new FirstResultWidget(Driver);
        }
    }
}
