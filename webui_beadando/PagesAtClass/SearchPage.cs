﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webui_beadando.WidgetAtClass;
using webui_beadando.WidgetsAtClass;

namespace webui_beadando.PagesAtClass
{
    public class SearchPage : BasePage
    {
        public SearchPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static SearchPage Navigate(IWebDriver webDriver)
        {
            webDriver.Navigate().GoToUrl("https://www.emag.hu");
            return new SearchPage(webDriver);
        }

        //public SearchWidget GetSearchWidget(String input)
        //{
        //    return new SearchWidget(Driver, input);
        //}

        public SearchResultWidget GetResultWidget()
        {
            return new SearchResultWidget(Driver);
        }

        public FirstResultWidget GetFirstResultWidget()
        {
            return new FirstResultWidget(Driver);
        }

        public WishListWidget GetWishLishWidget()
        {
            return new WishListWidget(Driver);
        }
    }
}
